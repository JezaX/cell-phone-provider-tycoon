﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {
    World gameWorld;
    Citizen[] Citizens;
    public float DaySpeed;
    [SerializeField] Slider timeSlider;
    [SerializeField] Text dayText;
    [SerializeField] Text populationText;

    private void Awake()
    {
        Citizens = new Citizen[10000000];

        for (int i = 0; i < Citizens.Length; i++)
        {
            Citizens[i] = new Citizen();
        }

        gameWorld = new World();
    }

    private void FixedUpdate()
    {
        DaySpeed = timeSlider.value;
        dayText.text = "Day: " + gameWorld.Day(DaySpeed);
        populationText.text = "Population: " + gameWorld.RegionPopulation(Citizens);
    }
}
