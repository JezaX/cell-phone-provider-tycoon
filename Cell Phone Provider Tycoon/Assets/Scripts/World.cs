﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World {
    float dayCount;

    public int Day(float speed)
    {
        dayCount += speed * Time.deltaTime;
        int day = (int)Mathf.Round(dayCount);
        return day;
    }

    public int RegionPopulation(Citizen[] population)
    {
        int regionPopulation = population.Length;
        return regionPopulation;
    }
}
